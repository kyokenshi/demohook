import React, { useEffect, useState } from 'react';
import './App.scss'
import queryString from 'query-string';
import Pagination from './components/Pagination';
import Postlist from './components/PostList';
import TodoForm from './components/TodoForm';
import TodoList from './components/TodoList';
import PostFilterForm from './components/PostFilterForm';



function App() {
  const [todoList, setTodoList] = useState(
    [
      { id: 1, title: 'i am  trumdle!' },
      { id: 2, title: 'i love donal trum!' },
      { id: 3, title: 'i love biden!' },
    ]
  );

  //api
  const [postList, setPostList] = useState([]);
  const [pagination, setPagination] = useState({
    _page: 1,
    _limit: 10,
    _totalRows: 1,
  });

  const [filters, setFilters] = useState({
    _limit: 10,
    _page: 1,
  })

  useEffect(() => {
    async function fetchPostList() {
      try {
        //...

        <div class="panel panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title">Panel title</h3>
          </div>
          <div class="panel-body">
            Panel content
            </div>
        </div>
        const paramsString = queryString.stringify(filters);
        const requesUrl = `http://js-post-api.herokuapp.com/api/posts?${paramsString}`;
        const response = await fetch(requesUrl);
        const responseJson = await response.json();
        console.log({ responseJson })

        const { data, pagination } = responseJson;
        setPostList(data);
        setPagination(pagination);

      } catch (error) {
        console.log('Failed to fetch post list', error.message)
      }
    }
    fetchPostList();
  }, [filters])


  function handlePageChange(newPage) {
    console.log('New page:', newPage);
    setFilters({
      ...filters,
      _page: newPage,
    });
  }


  function handleTodoClick(todo) {
    console.log(todo)
    const index = todoList.findIndex(x => x.id == todo.id);
    if (index < 0) return;
    const newTodoList = [...todoList];
    newTodoList.splice(index, 1);
    setTodoList(newTodoList);
  }


  function handleTodoOnSubmit(formValues) {
    console.log('Form submit: ', formValues);
    // add todo current todo list
    const newTodo = {
      id: todoList.length + 1,
      ...formValues,
    }
    const newTodoList = [...todoList];
    newTodoList.push(newTodo)
    setTodoList(newTodoList)
  };

  function handleFilerChange(newFilter) {
    console.log('New filter :', newFilter)
  }

  return (
    <div className="App">
      <h1>Welcome to Post List</h1>
      {/* <TodoForm onSubmit={handleTodoOnSubmit} /> */}
      {/* <TodoList todos={todoList} onTodoClick={handleTodoClick} /> */}
      <PostFilterForm
        onSubmit={handleFilerChange}>

      </PostFilterForm>
      <Postlist posts={postList} />
      <Pagination
        pagination={pagination}
        onPageChange={handlePageChange}

      />
    </div>
  );
}

export default App;
